package ar.edu.unq.libromatrizdigital.persistence;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

import ar.edu.unq.libromatrizdigital.hibernate.OperationHome;
import ar.edu.unq.libromatrizdigital.hibernate.SessionFactoryContainer;
import ar.edu.unq.libromatrizdigital.model.Alumno;
import ar.edu.unq.libromatrizdigital.model.Carrera;
import ar.edu.unq.libromatrizdigital.model.Cursada;
import ar.edu.unq.libromatrizdigital.model.Direccion;
import ar.edu.unq.libromatrizdigital.model.Docente;
import ar.edu.unq.libromatrizdigital.model.EstadoCursada;
import ar.edu.unq.libromatrizdigital.model.Materia;

public class GenerateData {

	public static void main(String[] args) {

		SessionFactoryContainer.buildSessionFactory(true);
		Session s = SessionFactoryContainer.getSessionFactory().getCurrentSession();

		Transaction transaction = s.beginTransaction();

		try {
			
			Direccion direccion1 = new Direccion("Alem", "1020", "Capitán Sarmiento");
			Direccion direccion2 = new Direccion("Guido Lucotti", "1355", "Capitán Sarmiento");
			Direccion direccion3 = new Direccion("Santiago Spinetta", "1421", "Capitán Sarmiento");
			OperationHome.getInstance().insert(direccion1);
			OperationHome.getInstance().insert(direccion2);
			OperationHome.getInstance().insert(direccion3);
			
			Carrera carrera1 = new Carrera("Profesorado de Educación Inicial", "Res.2017", 5);
			Carrera carrera2 = new Carrera("Profesorado de Educación Primaria", "Res.2017", 5);
			Carrera carrera3 = new Carrera("Profesorado de Educación Especial", "Res.2017", 5);
			OperationHome.getInstance().insert(carrera1);
			OperationHome.getInstance().insert(carrera2);
			OperationHome.getInstance().insert(carrera3);
			
			Docente docente1 = new Docente("Leonardo", "Gassmann", direccion1, "02478-457272");
			Docente docente2 = new Docente("Federico", "Aloi", direccion2, "02478-457373");
			Docente docente3 = new Docente("Pablo", "Nielou", direccion3, "02478-457474");
			OperationHome.getInstance().insert(docente1);
			OperationHome.getInstance().insert(docente2);
			OperationHome.getInstance().insert(docente3);
			
			Alumno alumno1 = new Alumno("Mario", "Gudiño", "30773213", "458789", direccion3,"mgudinio@gmail.com");
			OperationHome.getInstance().insert(alumno1);
			//OperationHome.getInstance().insert(alumno1, carrera1); Como hago apra cargar un alumno a una carrera
			alumno1.addCarreras(carrera1);
			carrera1.agregarAlumno(alumno1);
			
			Materia materia1 = new Materia(carrera1, "Psicologia Social e Institucional", true);
			OperationHome.getInstance().insert(materia1);
			
			Materia materia2 = new Materia(carrera1, "Matemática", true);
			OperationHome.getInstance().insert(materia2);
			
			carrera1.agregarMateria(materia2);
			carrera1.agregarMateria(materia1);
			
			Cursada cursada1 = new Cursada(materia1, 8.5, EstadoCursada.APROBADA_FINAL, 2017);
			OperationHome.getInstance().insert(cursada1);
			
			transaction.commit();
		} catch (RuntimeException e) {
			transaction.rollback();
			throw e;
		} finally {
			s.close();
			SessionFactoryContainer.getSessionFactory().close();
		}

	}

	protected static void setUp() throws Exception {
		// A SessionFactory is set up once for an application!
		final StandardServiceRegistry registry = new StandardServiceRegistryBuilder().configure().build();

		try {
			SessionFactory sessionFactory = new MetadataSources(registry).buildMetadata().buildSessionFactory();
		} catch (Exception e) {
			// The registry would be destroyed by the SessionFactory, but we had trouble
			// building the SessionFactory
			// so destroy it manually.
			StandardServiceRegistryBuilder.destroy(registry);
			throw e;
		}
	}

}
