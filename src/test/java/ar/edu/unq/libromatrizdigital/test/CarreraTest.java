package ar.edu.unq.libromatrizdigital.test;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;

import org.junit.Before;
import org.junit.Test;

import ar.edu.unq.libromatrizdigital.model.Carrera;
import ar.edu.unq.libromatrizdigital.model.OfertaAcademicaStore;

public class CarreraTest {

	private Carrera informatica;
	private Carrera contador;
	private Carrera diseno;

	public CarreraTest() {

	}

	@Before
	public void setUp() {
		informatica = new Carrera("tecnicatura en informatica", "1270/8", 5);
		contador = new Carrera("Contador", "12580/9", 6);
		diseno = new Carrera("Diseño grafico", "1350/17", 5);

		OfertaAcademicaStore.instituto().agregarCarreraAlListado(informatica);
		OfertaAcademicaStore.instituto().agregarCarreraAlListado(contador);
		OfertaAcademicaStore.instituto().agregarCarreraAlListado(diseno);
	}

//	@Test
//	public void trasArchivarCarreraNoApareceEnElListado() {
//		OfertaAcademica.instituto().archivarCarrera(diseno);
//
//		assertEquals(Arrays.asList(informatica, contador), OfertaAcademica.instituto().getCarreras());
//		assertEquals(Arrays.asList(diseno), OfertaAcademica.instituto().getCarrerasArchivadas());
//	}

//	@Test
//	public void ArchivandoLaCarreraApareceEnElListadoDeCarrerasArchivadas() {
//		OfertaAcademica.instituto().archivarCarrera(diseno);
//
//		assertEquals(Arrays.asList(diseno), OfertaAcademica.instituto().getCarrerasArchivadas());
//
//	}

	@Test
	public void eliminandoUnaCarreraArchivadaDejaDeAparecenEnElListadoDeCarrerasArchivadas() {
		OfertaAcademicaStore.instituto().archivarCarrera(informatica);
		OfertaAcademicaStore.instituto().archivarCarrera(contador);
		OfertaAcademicaStore.instituto().borrarCarrera(informatica);

		assertEquals(Arrays.asList(contador), OfertaAcademicaStore.instituto().getCarrerasArchivadas());
	}

//	@Test
//	public void AlDesarchivarUnaCarreraVuelveAlListadoDeCarrerasActivas() {
//		OfertaAcademica.instituto().archivarCarrera(informatica);
//		OfertaAcademica.instituto().archivarCarrera(contador);
//		OfertaAcademica.instituto().activarCarreraArchivada(contador);
//
//		assertEquals(Arrays.asList(diseno, contador), OfertaAcademica.instituto().getCarreras());
//	}
}
