package ar.edu.unq.libromatrizdigital.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

@Entity
public class Carrera extends Persistible {
	
	@Column(nullable=false)
	private String nombre;
	
	@Column(nullable=false)
	private String resolucion;
	
	private double duracion;
	
	@Transient
	private Materia materia;
	
	@ManyToMany(cascade = { CascadeType.ALL })
    @JoinTable(
        name = "Alumno_Carrera", 
        joinColumns = { @JoinColumn(name = "Alumno_id") }, 
        inverseJoinColumns = { @JoinColumn(name = "carreras_id") }
    )
	private List<Alumno> alumnosInscriptos = new ArrayList<>();
	
	@OneToMany(cascade = { CascadeType.ALL })
	private List<Materia> listadoMaterias = new ArrayList<>();
	
	public Carrera(String string) { }
	
	public Carrera() { }
	
	public Carrera(String nombre, String resolucion, double duracion) {
		super();
		this.nombre = nombre;
		this.resolucion = resolucion;
		this.duracion = duracion;
	}

	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getResolucion() {
		return resolucion;
	}
	public void setResolucion(String resolucion) {
		this.resolucion = resolucion;
	}

	public double getDuracion() {
		return duracion;
	}

	public void setDuracion(double duracion) {
		this.duracion = duracion;
	}
	public List<Materia> getMaterias() {
		return listadoMaterias;
	}
	public boolean yaFueCargadaMateria(String nombre){
		return listadoMaterias.stream().anyMatch(m -> m.getNombre() == nombre);
	}
	public boolean puedoAgregarMateria(Materia materia){
		return !yaFueCargadaMateria(materia.getNombre());
	}
	
	public void agregarMateria(Materia materia) {
		if(puedoAgregarMateria(materia)){
			this.listadoMaterias.add(materia);
			materia.setCarrera(this);
		}
	}


	public List<Alumno> getAlumnosInscriptos() {
		return alumnosInscriptos;
	}

	public void setAlumnosInscriptos(Alumno alumnosInscriptos) {
		this.alumnosInscriptos.add(alumnosInscriptos);
	}
	
	public Materia getMateriaConNombre(String name) {
		return listadoMaterias.stream().filter(c -> c.getNombre().toLowerCase().equals(name.toLowerCase())).findAny().get();
	}

	public void agregarAlumno(Alumno alumno) {
		this.alumnosInscriptos.add(alumno);
	}	

	public List<Materia> getListadoMaterias() {
		return listadoMaterias;
	}

	public boolean contieneMateria(Materia materia) {
		return this.getMaterias().contains(materia);
	}

	public void setListadoMaterias(List<Materia> listadoMaterias) {
		this.listadoMaterias = listadoMaterias;
	}
}
