package ar.edu.unq.libromatrizdigital.model;

public enum EstadoCursada {
	CURSANDO, REGULARIZADA, APROBADA_FINAL, APROBADA_PROMOCION, DESAPROBADA
}
