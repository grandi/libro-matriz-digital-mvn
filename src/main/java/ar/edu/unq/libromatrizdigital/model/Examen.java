package ar.edu.unq.libromatrizdigital.model;

public class Examen {

	private String examenFinal;

	public Examen(String _examenFinal) {
		this.setExamenFinal(_examenFinal);
	}

	public String getExamenFinal() {
		return examenFinal;
	}

	public void setExamenFinal(String examenFinal) {
		this.examenFinal = examenFinal;
	}
}