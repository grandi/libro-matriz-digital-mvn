package ar.edu.unq.libromatrizdigital.model;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.Transient;

import ar.edu.unq.libromatrizdigital.wicket.alumno.AlumnoStore;

@Entity
public class Alumno extends Persistible{
	private static final long serialVersionUID = -8385864529236314139L;
	
	private String nombre;
	private String apellido;
	private String dni;
	
	@OneToOne
	private Direccion direccion;
	
	private String telefono;
	private String email;
	
	@Transient
	private List<Cursada> cursadas = new ArrayList<>();
	@Transient
	private static AlumnoStore unoEspecifico;
	@ManyToMany(fetch = FetchType.EAGER)
	private List<Carrera> carreras = new ArrayList<>();

	public Alumno() {
	}

	public Alumno(String _nombre, String _apellido, String _dni) {
		this.nombre = _nombre;
		this.apellido = _apellido;
		this.dni = _dni;
	}

	public Alumno(String _nombre, String _apellido, String _dni, String _telefono, Direccion _direccion,
			String _email) {
		this.nombre = _nombre;
		this.apellido = _apellido;
		this.dni = _dni;
		this.telefono = _telefono;
		this.direccion = _direccion;
		this.email = _email;
	}

	public String getNombreCompleto() {
		return this.getApellido() + ", " + this.getNombre();
	}

	public boolean estaCursando() {
		return unoEspecifico.getCursadasDe(this).stream().anyMatch(c -> c.estadoCursando());

	}

	public boolean puedeMatricularseA(Materia materia) {
		return materia.getCorrelativas().stream().allMatch(m -> this.estaRegularizada(m));
	}

	private boolean estaRegularizada(Materia m) {
		return this.cursadas.stream().filter(c -> c.getMateria().equals(m))
				.anyMatch(c -> c.estadoRegularizadoOAprobado());
	}

	public Set<Materia> getMateriasCursadasEnLaCarrera(Carrera carrera) { // Y
																			// tiene
																			// las
																			// materias
																			// regularizadas
		return this.getCursadasEnLaCarrera(carrera).stream().map(c -> c.getMateria()).collect(Collectors.toSet());
	}

	public Set<Materia> getMateriasPorCursarEnCarrera(Carrera carrera) {
		return SetUtils.diferencia(new HashSet<>(carrera.getMaterias()), this.getMateriasCursadasEnLaCarrera(carrera));
	}

	public List<Cursada> getCursadasEnLaCarrera(Carrera carrera) {
		return this.cursadas.stream().filter(c -> carrera.contieneMateria(c.getMateria())).collect(Collectors.toList());
	}

	private Carrera getCarrera(Carrera carrera) {
		return this.carreras.stream().filter(c -> c.getNombre().equals(carrera.getNombre())).findAny().get();
	}

	public List<Cursada> getMateriasCursadasPor(String _dni) {// hay otra opcion
																// desde store
		return this.cursadas.stream()
				.filter(c -> c.cursaAlumnoConDni(_dni) && (c.estadoCursando() || c.estadoRegularizadoOAprobado()))
				.collect(Collectors.toList());
	}

	public List<Cursada> getMateriasCursadas() {
		return this.cursadas.stream().filter(c -> c.estadoCursando()).collect(Collectors.toList());
	}

	public void agregarCursada(Cursada cursada) {
		this.cursadas.add(cursada);
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public String getDni() {
		return dni;
	}

	public void setDni(String dni) {
		this.dni = dni;
	}

	public Direccion getDireccion() {
		return direccion;
	}

	public void setDireccion(Direccion direccion) {
		this.direccion = direccion;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public void setCursadas(List<Cursada> cursadas) {
		this.cursadas = cursadas;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;

	}

	public List<Carrera> getCarreras() {
		return carreras;
	}

	public void setCarreras(List<Carrera> carreras) {
		this.carreras = carreras;
	}

	public void addCarreras(Carrera _carrera) {
		this.carreras.add(_carrera);

	}

	public List<Cursada> getCursadas() {
		return cursadas;
	}

	// Cursadas no rendidas
	public List<Cursada> getCursadasEnCursoORegularizadasPor(String dni) {
		return this.getCursadas().stream().filter(cursada -> cursada.estadoCursando() || cursada.estaRegularizada())
				.collect(Collectors.toList());
	}

	// Puede inscribirse a finales cuyas correlativas se encuentren regularizadas o
	// aprobadas
	public boolean puedeInscribirseAFinalDe(Materia laMateria) {
		return laMateria.getCorrelativas().stream().allMatch(materia -> this.estaAprobada(materia));
	}

	private boolean estaAprobada(Materia materia) {
		return this.cursadas.stream().filter(cursada -> cursada.getMateria().equals(materia))
				.anyMatch(cursada -> cursada.estaAprobada());
	}

}