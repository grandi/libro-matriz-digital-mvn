package ar.edu.unq.libromatrizdigital.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.hibernate.Session;

import ar.edu.unq.libromatrizdigital.hibernate.OperationHome;

public class SessionAttacher {
	
	static final String attributeName = "listAttach";
	static private SessionAttacher instance = new SessionAttacher();

	private ThreadLocal<HttpSession> threadLocal = new ThreadLocal<>();
	
	public static SessionAttacher getInstance() {
		return instance;
	}
	
	private HttpSession getHttpSession(){
		return threadLocal.get();
	}
	
	private void setHttpSession(HttpSession http){
		threadLocal.set(http);
	}
	
	public void initSession(Session session, HttpSession http){
		this.setHttpSession(http);
		List<?> list = (ArrayList<?>) http.getAttribute(attributeName);
		if(list != null){
			list.stream().forEach(o -> session.update(o));
			http.setAttribute(attributeName, null);
		}
	}
	
	public void attach(Serializable o){
			List<Serializable> list = (ArrayList<Serializable>) getHttpSession().getAttribute(attributeName);
			if(list == null){
				list = new ArrayList<>();
				getHttpSession().setAttribute(attributeName, list);
			}
			list.add(o);
	}
	
}
