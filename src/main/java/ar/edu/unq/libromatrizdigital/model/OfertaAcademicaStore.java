package ar.edu.unq.libromatrizdigital.model;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;

import ar.edu.unq.libromatrizdigital.hibernate.OperationHome;
import ar.edu.unq.libromatrizdigital.hibernate.SessionFactoryContainer;

public class OfertaAcademicaStore {
	public static OfertaAcademicaStore elUnico;

	private List<Carrera> carreras = new ArrayList<>();
	private List<Carrera> carrerasArchivadas = new ArrayList<>();
	public List<Alumno> alumnos;

	public static OfertaAcademicaStore instituto() {
		if (elUnico == null) {
			elUnico = new OfertaAcademicaStore();
		}
		return elUnico;
	}

	public List<Carrera> getCarreras() {
		Session s = SessionFactoryContainer.getSessionFactory().getCurrentSession();
		return s.createQuery("from Carrera", Carrera.class).list();
		//return this.carreras;
	}

	public void agregarCarreraAlListado(Carrera carrera) {
		this.carreras.add(carrera);
		OperationHome.getInstance().insert(carrera);
	}

	public Carrera getBuscarCarrera(Carrera carrera) {
		return this.carreras.stream().filter(c -> c.getNombre().equals(carrera)).findAny().get();
	}

	public boolean carreraArchivada(Carrera carre) {
		return this.carrerasArchivadas.stream().anyMatch(car -> car.getNombre().equals(carre));
	}

	public void archivarCarrera(Carrera carreraAArchivar) {
		this.carrerasArchivadas.add(carreraAArchivar);
		this.carreras.remove(carreraAArchivar);
	}

	public void borrarCarrera(Carrera borrarCarrera) {
		this.carrerasArchivadas.remove(borrarCarrera);
	}

	public void activarCarreraArchivada(Carrera carrera) {
		this.carrerasArchivadas.remove(carrera);
		this.carreras.add(carrera);

	}

	public Carrera getCarreraConNombre(String nombre) {
		return this.carreras.stream().filter(c -> c.getNombre().toLowerCase().equals(nombre.toLowerCase())).findAny()
				.get();
	}

	public void carreraInicial() {
		Carrera tInformatica = new Carrera("tecnicatura en informatica", "1270/8", 5);
		Carrera contador = new Carrera("Contador", "12580/9", 6);
		Carrera diseno = new Carrera("Diseño grafico", "1350/17", 5);
		Carrera analista = new Carrera("Analista programador", "12782", 3);
		this.agregarCarreraAlListado(diseno);
		this.agregarCarreraAlListado(contador);
		this.agregarCarreraAlListado(tInformatica);
		this.agregarCarreraAlListado(analista);
		Alumno agustina = new Alumno("Agustina", "Ponce", "28190283", "2478-448792",
				new Direccion("Pellegrin", "960", "Capitan Sarmiento"), "agustina@hotmail.com");
		Alumno jose = new Alumno("jose", "gonzalez", "31872561", "2478-502480",
				new Direccion("Santiago del Estero", "_700", "La Luisa"), "jose@gmail.com");
		diseno.setAlumnosInscriptos(agustina);
		analista.setAlumnosInscriptos(jose);
		tInformatica.setAlumnosInscriptos(jose);
		contador.setAlumnosInscriptos(agustina);
	}

	public List<Carrera> getCarrerasArchivadas() {
		return this.carrerasArchivadas;
	}

	public Carrera getCarrera(Carrera carrera) {
		return this.carreras.stream().filter((car) -> car.getNombre().equals(carrera)).findAny().get();

	}

	public void modificarCarrera(Carrera carrera) {
		OperationHome.getInstance().update(carrera);
	}

}