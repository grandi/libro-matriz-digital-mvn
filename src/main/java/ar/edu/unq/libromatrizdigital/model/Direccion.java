package ar.edu.unq.libromatrizdigital.model;

import javax.persistence.Entity;

@Entity
public class Direccion extends Persistible  {
	private static final long serialVersionUID = -1542640865399851934L;
	
	private String calle;
	private String numero;
	private String localidad;

	public Direccion() { }

	public Direccion(String _calle, String _numero, String _localidad) {
		this.calle = _calle;
		this.numero = _numero;
		this.localidad = _localidad;
	}

	public String getCalle() {
		return calle;
	}

	public void setCalle(String calle) {
		this.calle = calle;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getLocalidad() {
		return localidad;
	}

	public void setLocalidad(String localidad) {
		this.localidad = localidad;
	}

}
