package ar.edu.unq.libromatrizdigital.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.OneToOne;
import javax.persistence.Transient;

@Entity
public class Cursada extends Persistible {

	@OneToOne
	private Materia materia;

	private double notaFinal;

	@Enumerated(EnumType.STRING)
	private EstadoCursada estado;

	private int anio;

	@Transient
	private List<Alumno> quienesCursaron = new ArrayList<>();

	public Cursada() {
		super();
	}

	public Cursada(EstadoCursada _estadoCursada) {
		this.estado = _estadoCursada;
	}

	public Cursada(Materia materia, EstadoCursada estado) {
		super();
		this.materia = materia;
		this.estado = estado;
	}

	public Cursada(Materia materia, double notaFinal, EstadoCursada estado, int anio) {
		super();
		this.materia = materia;
		this.notaFinal = notaFinal;
		this.estado = estado;
		this.anio = anio;
	}

	public void setEstado(EstadoCursada _estadoCursada) {
		this.estado = (_estadoCursada);
	}

	public EstadoCursada getEstado() {
		return estado;
	}

	public double getNotaFinal() {
		return notaFinal;
	}

	public void setNotaFinal(double notaFinal) {
		this.notaFinal = notaFinal;
	}

	public int getAnio() {
		return anio;
	}

	public void setAnio(int anio) {
		this.anio = anio;
	}

	public void setQuienesCursaron(List<Alumno> quienesCursaron) {
		this.quienesCursaron = quienesCursaron;
	}

	public void addQuienHizoCursada(Alumno _alumno) {
		this.quienesCursaron.add(_alumno);
	}

	public List<Alumno> getQuienesCursaron() {
		return quienesCursaron;
	}

	public boolean cursaAlumnoConDni(String _dni) {
		return quienesCursaron.stream().anyMatch(a -> a.getDni().equals(_dni));
	}

	public boolean estadoCursando() {
		return this.getEstado().equals(EstadoCursada.CURSANDO);
	}

	public boolean estadoRegularizadoOAprobado() {
		return (this.getEstado().equals(EstadoCursada.REGULARIZADA)
				|| this.getEstado().equals(EstadoCursada.APROBADA_FINAL)
				|| this.getEstado().equals(EstadoCursada.APROBADA_PROMOCION));
	}

	public Materia getMateria() {
		return materia;
	}

	public void setMateria(Materia materia) {
		this.materia = materia;
	}

	public boolean estaRegularizada() {
		return this.getEstado().equals(EstadoCursada.REGULARIZADA);
	}

	public boolean estaAprobada() {
		return (this.getEstado().equals(EstadoCursada.APROBADA_PROMOCION)
				|| this.getEstado().equals(EstadoCursada.APROBADA_FINAL));
	}

	public boolean estaDesaprobada() {
		return this.getEstado().equals(EstadoCursada.DESAPROBADA);
	}

}
