package ar.edu.unq.libromatrizdigital.wicket.alumno;

import java.util.List;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.PropertyModel;

import ar.edu.unq.libromatrizdigital.model.Alumno;
import ar.edu.unq.libromatrizdigital.model.Carrera;
import ar.edu.unq.libromatrizdigital.wicket.HomePage;
import ar.edu.unq.libromatrizdigital.wicket.carrera.AgregarCarreraController;

public class ListadoAlumnoPage extends WebPage {
	private static final long serialVersionUID = -4444795484673343576L;

	private ListadoAlumnoController controller = new ListadoAlumnoController();
	private AgregarCarreraController carrera;
	private List<Alumno> alumnos;

	public ListadoAlumnoPage() {
		super();
		this.controller = new ListadoAlumnoController();
		this.agregarCuadroDeBusquedaPorDNI();
		this.agregarListado();
		this.agregarBotonVolver();
	}

	public ListadoAlumnoPage(Carrera carrera) {
		this.controller = new ListadoAlumnoController(carrera);
		this.agregarCuadroDeBusquedaPorDNI();
		this.agregarListado();
		this.agregarBotonVolver();
	}
	
	public ListadoAlumnoPage(String dniOApellido) {
		this.controller = new ListadoAlumnoController(dniOApellido);
		this.agregarCuadroDeBusquedaPorDNI();
		this.agregarListado();
		this.agregarBotonVolver();
	}
	

	private void agregarCuadroDeBusquedaPorDNI() {
		Form form = new Form("formBuscarDniOApellido") {
			private static final long serialVersionUID = -4770967393266976612L;

			@Override
			protected void onSubmit() {
				this.setResponsePage(new ListadoAlumnoPage(controller.getDniAlumno()));
			}
		};
		form.add(new TextField<>("textInput", new PropertyModel<>(controller, "dniAlumno")));

		this.add(form);
	}
	
	private void agregarListado() {
		this.add(new ListView<Alumno>("filaAlumno", new PropertyModel<>(this.controller, "alumnos")) {

			private static final long serialVersionUID = 2426749934569985837L;

			protected void populateItem(ListItem<Alumno> panel) {
				Alumno alumno = panel.getModelObject();
				CompoundPropertyModel<Alumno> alumnoModel = new CompoundPropertyModel<>(alumno);
				panel.add(new Label("nombreCompleto", alumnoModel.bind("nombreCompleto")));
				panel.add(new Label("dni", alumnoModel.bind("dni")));

				panel.add(new Link<String>("eliminarAlumno") {
					private static final long serialVersionUID = 8397905807894993988L;

					@Override
					public void onClick() {
						this.setResponsePage(new EliminarAlumnoPage(alumno));

					}

				});

				panel.add(new Link<String>("verDatos") {
					private static final long serialVersionUID = -6226604951323971752L;

					@Override
					public void onClick() {
						this.setResponsePage(new DatosAlumnoPage(alumno));
					}

				});

			};

		});
	}

	private void agregarBotonVolver() {
		this.add(new Link<String>("agregarAlumno") {

			private static final long serialVersionUID = -4267458418903930966L;

			@Override
			public void onClick() {
				this.setResponsePage(new AgregarAlumnoPage());

			}

		});

		this.add(new Link<String>("volver") {

			private static final long serialVersionUID = 505927122883116822L;

			@Override
			public void onClick() {
				this.setResponsePage(new HomePage());
			}
		});

	}

}
