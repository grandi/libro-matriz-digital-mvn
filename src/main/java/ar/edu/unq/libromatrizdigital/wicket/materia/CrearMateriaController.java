package ar.edu.unq.libromatrizdigital.wicket.materia;

import ar.edu.unq.libromatrizdigital.model.Carrera;
import ar.edu.unq.libromatrizdigital.model.Materia;

public class CrearMateriaController {
	private String nombre;
	private Boolean esPromocionable;

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Boolean getEsPromocionable() {
		return esPromocionable;
	}

	public void setEsPromocionable(Boolean esPromocionable) {
		this.esPromocionable = esPromocionable;
	}

	public void asignarMateria(Carrera carrera) {
		if (this.nombre != null && this.esPromocionable != null) {
			carrera.agregarMateria(new Materia(nombre, esPromocionable));
		}
	}

}
