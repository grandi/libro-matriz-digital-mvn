package ar.edu.unq.libromatrizdigital.wicket;

import org.hibernate.Transaction;

import org.apache.wicket.Page;
import org.apache.wicket.protocol.http.WebApplication;
import org.hibernate.Session;

import ar.edu.unq.libromatrizdigital.hibernate.SessionFactoryContainer;
import ar.edu.unq.libromatrizdigital.model.OfertaAcademicaStore;
import ar.edu.unq.libromatrizdigital.wicket.alumno.AlumnoStore;
import ar.edu.unq.libromatrizdigital.wicket.materia.MateriasStore;

public class LibroMatrizApplication extends WebApplication {

	@Override
	public Class<? extends Page> getHomePage() {
		return HomePage.class;
	}

	@Override
	protected void init() {
		super.init();
		System.out.println(" LEVANTANDO LA APP  ***********************");
		SessionFactoryContainer.buildSessionFactory(false);
		this.getMarkupSettings().setDefaultMarkupEncoding("UTF-8");
		this.getRequestCycleSettings().setResponseRequestEncoding("UTF-8");

//		Session s = SessionFactoryContainer.getSessionFactory().getCurrentSession();
//		Transaction tx = s.beginTransaction();
//		OfertaAcademicaStore.instituto().carreraInicial();
//		MateriasStore.unico().cargarDatosIniciales();
//		AlumnoStore.unico().cargarDatosIniciales();
//		tx.commit();
	}
}
