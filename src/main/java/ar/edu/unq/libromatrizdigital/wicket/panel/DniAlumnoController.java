package ar.edu.unq.libromatrizdigital.wicket.panel;

import java.io.Serializable;

import ar.edu.unq.libromatrizdigital.hibernate.OperationHome;
import ar.edu.unq.libromatrizdigital.model.Alumno;
import ar.edu.unq.libromatrizdigital.wicket.alumno.AlumnoStore;

public class DniAlumnoController implements Serializable{

	private Alumno alumno;
	private String dni;

	public DniAlumnoController() { }

	public DniAlumnoController(Alumno alumno) {
		this.alumno = alumno;
	}

	public void setAlumno(Alumno alumno) {
		this.alumno = alumno;
	}

	public void setDni(String dni) {
		this.dni = dni;
	}

	public String getDni() {
		return dni;
	}

	public Alumno getAlumno() {
//		Alumno alumno = AlumnoStore.unico().getAlumnoConDni(this.getDni());
//		OperationHome.getInstance().update(alumno);
//		return alumno;
//		esto lo comento porque puede ser un buen ejemplo de donde insertar el 
//		No me sale el nombre cuando un objeto no queres que muera
		return AlumnoStore.unico().getAlumnoConDni(this.getDni());
	}
	
}
