package ar.edu.unq.libromatrizdigital.wicket.matriculacion;

import java.io.Serializable;

import ar.edu.unq.libromatrizdigital.model.Carrera;
import ar.edu.unq.libromatrizdigital.model.Materia;

public class CrearMatriculacionController implements Serializable{

	private Materia materia;
	private Carrera carrera;

	public Materia getMateria() {
		return materia;
	}

	public void setMateria(Materia materia) {
		this.materia = materia;
	}

	// public List<Materia> getMateriasNoRegularizadas(){
	// return getCarrera().getMaterias.stream.filter(m ->
	// m.getMateriasNoRegularizadas).Collectors.toset();
	// }

	public Carrera getCarrera() {
		return carrera;
	}

	public void setCarrera(Carrera carrera) {
		this.carrera = carrera;
	}
}
