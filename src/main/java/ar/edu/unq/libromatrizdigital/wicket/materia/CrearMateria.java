package ar.edu.unq.libromatrizdigital.wicket.materia;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.model.PropertyModel;

import ar.edu.unq.libromatrizdigital.model.Carrera;

public class CrearMateria extends WebPage {
	private static final long serialVersionUID = 3341132595660337349L;
	private CrearMateriaController controller = new CrearMateriaController();
	private Carrera carrera;

	public CrearMateria(Carrera carrera) {
		super();
		this.carrera = carrera;
		this.agregarForm();
		this.agregarBotones();
	}

	private void agregarForm() {
		Form<CrearMateriaController> crearViajeForm = new Form<CrearMateriaController>("crearViajeForm") {
			private static final long serialVersionUID = -1309536194793150773L;

			@Override
			protected void onSubmit() {
				// accion de negocios
				CrearMateria.this.controller.asignarMateria(carrera);
				// navegacion
				this.setResponsePage(new MateriasPage(carrera));
			}
		};

		crearViajeForm.add(new TextField<>("origenCalle", new PropertyModel<>(this.controller, "nombre")));
		crearViajeForm.add(new TextField<>("origenNumero", new PropertyModel<>(this.controller, "esPromocionable")));

		this.add(crearViajeForm);
	}

	private void agregarBotones() {
		this.add(new Link<String>("volver") {
			private static final long serialVersionUID = -5103954084634722612L;

			@Override
			public void onClick() {
				this.setResponsePage(new MateriasPage(carrera));
			}
		});
	}
}