package ar.edu.unq.libromatrizdigital.wicket.panel;

import java.io.Serializable;
import java.util.List;

import ar.edu.unq.libromatrizdigital.hibernate.OperationHome;
import ar.edu.unq.libromatrizdigital.model.Alumno;
import ar.edu.unq.libromatrizdigital.model.Carrera;

public class CarrerasController implements Serializable{
	private Alumno alumno;
	private Carrera carrera;

	public Alumno getAlumno() {
		return alumno;
	}

	public void setAlumno(Alumno alumno) {
		this.alumno = alumno;
	}

	public Carrera getCarrera() {
		return carrera;
	}

	public void setCarrera(Carrera carrera) {
		Carrera carre = OperationHome.getInstance().findById(carrera.getId(), Carrera.class);
		this.carrera = carre;
	}

	public List<Carrera> getCarreras() {
		return this.getAlumno().getCarreras();
	}
}
