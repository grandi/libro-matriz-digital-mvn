package ar.edu.unq.libromatrizdigital.wicket.carrera;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.model.PropertyModel;

import ar.edu.unq.libromatrizdigital.model.Carrera;
import ar.edu.unq.libromatrizdigital.wicket.BotonConfirmar;

public class EliminarCarreraPage extends WebPage {

	private static final long serialVersionUID = 1L;

	private EliminarCarreraController controller;

	public EliminarCarreraPage(Carrera carrera) {

		this.controller = new EliminarCarreraController(carrera);// CarreraController(carrera);
		this.eliminarLaCarrera();
	}

	public void eliminarLaCarrera() {
		Form<AgregarCarreraController> carrera = new Form<AgregarCarreraController>("eliminarLaCarrera") {

			private static final long serialVersionUID = 1L;

			@Override
			protected void onSubmit() {
				EliminarCarreraPage.this.controller.eliminarCarrera();
				this.setResponsePage(new ListadoCarrerasArchivadas());

			}
		};
		carrera.add(new BotonConfirmar("submit", "Está seguro que desea eliminar la carrera definitivamente?"));
		carrera.add(new TextField<>("nombre", new PropertyModel<>(this.controller, "nombre")));
		carrera.add(new TextField<>("resolucion", new PropertyModel<>(this.controller, "resolucion")));

		this.add(carrera);

		carrera.add(new Link<String>("cancelar") {

			private static final long serialVersionUID = 1L;

			@Override
			public void onClick() {
				this.setResponsePage(new ListadoCarrerasArchivadas());

			}

		});

	}

}
