package ar.edu.unq.libromatrizdigital.wicket.inscripcionafinal;

import ar.edu.unq.libromatrizdigital.model.Alumno;
import ar.edu.unq.libromatrizdigital.wicket.alumno.AlumnoStore;

public class DniAlumnoInscripcionAFinalController {

	private Alumno alumno;
	private String dni;

	public DniAlumnoInscripcionAFinalController() {	}

	public DniAlumnoInscripcionAFinalController(Alumno alumno) {
		this.alumno = alumno;
	}

	public void setAlumno(Alumno alumno) {
		this.alumno = alumno;
	}

	public void setDni(String dni) {
		this.dni = dni;
	}

	public String getDni() {
		return dni;
	}

	public Alumno getAlumno() {

		return AlumnoStore.unico().getAlumnoConDni(this.getDni());
	}
}
