package ar.edu.unq.libromatrizdigital.wicket.inscripcionafinal;

import ar.edu.unq.libromatrizdigital.model.Alumno;
import ar.edu.unq.libromatrizdigital.model.Carrera;
import ar.edu.unq.libromatrizdigital.model.Cursada;

public class ExamenInscripcionAFinalController {
	private Carrera carrera;
	private Cursada cursada;
	private Alumno alumno;
	private String fecha;
	private String hora;

	public ExamenInscripcionAFinalController() {
		super();
	}

	public ExamenInscripcionAFinalController(Carrera carrera, Cursada cursada, Alumno alumno, String fecha,
			String hora) {
		super();
		this.carrera = carrera;
		this.cursada = cursada;
		this.alumno = alumno;
		this.fecha = fecha;
		this.hora = hora;
	}

	public Carrera getCarrera() {
		return carrera;
	}

	public void setCarrera(Carrera carrera) {
		this.carrera = carrera;
	}

	public Cursada getCursada() {
		return cursada;
	}

	public void setCursada(Cursada cursada) {
		this.cursada = cursada;
	}

	public Alumno getAlumno() {
		return alumno;
	}

	public void setAlumno(Alumno alumno) {
		this.alumno = alumno;
	}

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	public String getHora() {
		return hora;
	}

	public void setHora(String hora) {
		this.hora = hora;
	}

}
