package ar.edu.unq.libromatrizdigital.wicket.carrera;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.PropertyModel;

import ar.edu.unq.libromatrizdigital.model.Carrera;

public class ListadoCarrerasArchivadas extends WebPage {

	private static final long serialVersionUID = 1L;
	private AgregarCarreraController controller;

	public ListadoCarrerasArchivadas() {
		this.controller = new AgregarCarreraController();
		this.tablaCarrerasArchivadas();
		this.botonSalir();
	}

	public void tablaCarrerasArchivadas() {
		this.add(new ListView<Carrera>("lasArchivadas", new PropertyModel<>(this.controller, "carrerasArchivadas")) {

			private static final long serialVersionUID = 1L;

			@Override
			protected void populateItem(ListItem<Carrera> item) {
				CompoundPropertyModel<Carrera> carreraArchivada = new CompoundPropertyModel<>(item.getModel());
				item.add(new Label("nombre", carreraArchivada.bind("nombre")));
				item.add(new Label("resolucion", carreraArchivada.bind("resolucion")));
				item.add(new Label("duracion", carreraArchivada.bind("duracion")));

				item.add(new Link<String>("borrarCarrera") {
					private static final long serialVersionUID = 1L;

					@Override
					public void onClick() {
						this.setResponsePage(new EliminarCarreraPage(item.getModelObject()));
					}

				});

				item.add(new Link<String>("desarchivarCarrera") {

					private static final long serialVersionUID = 1L;

					@Override
					public void onClick() {
						this.setResponsePage(new DesarchivarCarreraPage(item.getModelObject()));

					}

				});
			}
		});
	}

	public void botonSalir() {
		this.add(new Link<String>("salir") {

			private static final long serialVersionUID = 1L;

			@Override
			public void onClick() {
				this.setResponsePage(new ListadoDeCarreras());

			}

		});
	}

}
