package ar.edu.unq.libromatrizdigital.wicket.inscripcionafinal;

import ar.edu.unq.libromatrizdigital.model.Carrera;
import ar.edu.unq.libromatrizdigital.model.Materia;

public class CrearInscripcionAFinalController {

	private Materia materia;
	private Carrera carrera;

	public Materia getMateria() {
		return materia;
	}

	public void setMateria(Materia materia) {
		this.materia = materia;
	}

	public Carrera getCarrera() {
		return carrera;
	}

	public void setCarrera(Carrera carrera) {
		this.carrera = carrera;
	}

}
