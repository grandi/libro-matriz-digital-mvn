package ar.edu.unq.libromatrizdigital.wicket.matriculacion;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.link.Link;

import ar.edu.unq.libromatrizdigital.wicket.HomePage;
import ar.edu.unq.libromatrizdigital.wicket.panel.CarrerasPanel;
import ar.edu.unq.libromatrizdigital.wicket.panel.DniAlumnoPanel;
import ar.edu.unq.libromatrizdigital.wicket.panel.MateriasPanel;

public class FormMatriculacion extends WebPage {

	private static final long serialVersionUID = 1L;
	private CrearMatriculacionController controller;
	private DniAlumnoPanel dniAlumnoPanel;
	private CarrerasPanel carrerasPanel;
	private MateriasPanel materiasPanel;

	public FormMatriculacion(DniAlumnoPanel dniAlumnoPanel) {
		super();

		this.dniAlumnoPanel = dniAlumnoPanel;

		Form<CrearMatriculacionController> crearCursadaForm = new Form<CrearMatriculacionController>(
				"crearCursadaForm") {
			private static final long serialVersionUID = -1309536194793150773L;

			@Override
			protected void onSubmit() {
				// accion de negocios
				// navegacion
			}
		};

		this.add(crearCursadaForm);

		this.add(this.dniAlumnoPanel);

		this.carrerasPanel = new CarrerasPanel("carrerasPanel");
		this.add(this.carrerasPanel);

		this.materiasPanel = new MateriasPanel("materiasPanel");
		this.add(this.materiasPanel);

		this.dniAlumnoPanel.setCarreraPanel(carrerasPanel);
		this.carrerasPanel.setMateriasPanel(materiasPanel);

		this.add(new Link<String>("volver") {
			private static final long serialVersionUID = 1L;

			@Override
			public void onClick() {
				this.setResponsePage(new HomePage());
			}

		});

	}

	public FormMatriculacion() {
		super();

		Form<CrearMatriculacionController> crearCursadaForm = new Form<CrearMatriculacionController>(
				"crearCursadaForm") {
			private static final long serialVersionUID = -1309536194793150773L;

			@Override
			protected void onSubmit() {
				// accion de negocios
				// navegacion
			}
		};

		this.add(crearCursadaForm);

		// Se agrega el panel de dniAlumnoPanel en el Formulario de Matriculación
		this.dniAlumnoPanel = new DniAlumnoPanel("dniAlumnoPanel");
		this.add(this.dniAlumnoPanel);

		// Se agrega el panel de carrerasPanel en el Formulario de Matriculación
		this.carrerasPanel = new CarrerasPanel("carrerasPanel");
		this.add(this.carrerasPanel);

		this.materiasPanel = new MateriasPanel("materiasPanel");
		this.add(this.materiasPanel);

		this.dniAlumnoPanel.setCarreraPanel(carrerasPanel);
		this.carrerasPanel.setMateriasPanel(materiasPanel);

		this.add(new Link<String>("volver") {
			private static final long serialVersionUID = 1L;

			@Override
			public void onClick() {
				this.setResponsePage(new HomePage());
			}

		});

	}

}
