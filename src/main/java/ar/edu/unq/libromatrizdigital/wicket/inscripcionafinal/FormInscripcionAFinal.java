package ar.edu.unq.libromatrizdigital.wicket.inscripcionafinal;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.link.Link;

import ar.edu.unq.libromatrizdigital.wicket.HomePage;

public class FormInscripcionAFinal extends WebPage {
	private static final long serialVersionUID = -5185130375403129933L;

	private CrearInscripcionAFinalController controller;
	private DniAlumnoInscripcionAFinalPanel dniAlumnoInscripcionAFinalPanel;
	private CarrerasInscripcionAFinalPanel carrerasInscripcionAFinalPanel;
	private CursadasInscripcionAFinalPanel cursadasInscripcionAFinalPanel;
	private ExamenInscripcionAFinalPanel examenInscripcionAFinalPanel;

	public FormInscripcionAFinal(DniAlumnoInscripcionAFinalPanel dniAlumnoInscripcionAFinalPanel) {
		super();

		this.dniAlumnoInscripcionAFinalPanel = dniAlumnoInscripcionAFinalPanel;

		Form<CrearInscripcionAFinalController> crearCursadaForm = new Form<CrearInscripcionAFinalController>(
				"crearCursadaForm") {
			private static final long serialVersionUID = -1309536194793150773L;

			@Override
			protected void onSubmit() {
				// TODO
			}
		};

		this.add(crearCursadaForm);

		this.add(this.dniAlumnoInscripcionAFinalPanel);

		this.dniAlumnoInscripcionAFinalPanel = new DniAlumnoInscripcionAFinalPanel("dniAlumnoInscripcionAFinalPanel");
		this.add(this.dniAlumnoInscripcionAFinalPanel);

		this.carrerasInscripcionAFinalPanel = new CarrerasInscripcionAFinalPanel("carrerasInscripcionAFinalPanel");
		this.add(this.carrerasInscripcionAFinalPanel);

		this.cursadasInscripcionAFinalPanel = new CursadasInscripcionAFinalPanel("cursadasInscripcionAFinalPanel");
		this.add(this.cursadasInscripcionAFinalPanel);

		this.examenInscripcionAFinalPanel = new ExamenInscripcionAFinalPanel("examenInscripcionAFinalPanel");
		this.add(this.examenInscripcionAFinalPanel);

		this.dniAlumnoInscripcionAFinalPanel.setCarrerasInscripcionAFinalPanel(carrerasInscripcionAFinalPanel);
		this.carrerasInscripcionAFinalPanel.setCursadasInscripcionAFinalPanel(cursadasInscripcionAFinalPanel);

		this.add(new Link<String>("volver") {
			private static final long serialVersionUID = 1L;

			@Override
			public void onClick() {
				this.setResponsePage(new HomePage());
			}

		});

	}

	public FormInscripcionAFinal() {
		super();

		Form<CrearInscripcionAFinalController> crearCursadaForm = new Form<CrearInscripcionAFinalController>(
				"crearCursadaForm") {
			private static final long serialVersionUID = 4072131542339416351L;

			@Override
			protected void onSubmit() {
				// TODO
			}
		};

		this.add(new Link<String>("volver") {
			private static final long serialVersionUID = 1L;

			@Override
			public void onClick() {
				this.setResponsePage(new HomePage());
			}

		});

		this.add(crearCursadaForm);

		this.dniAlumnoInscripcionAFinalPanel = new DniAlumnoInscripcionAFinalPanel("dniAlumnoInscripcionAFinalPanel");
		this.add(this.dniAlumnoInscripcionAFinalPanel);

		this.carrerasInscripcionAFinalPanel = new CarrerasInscripcionAFinalPanel("carrerasInscripcionAFinalPanel");
		this.add(this.carrerasInscripcionAFinalPanel);

		this.cursadasInscripcionAFinalPanel = new CursadasInscripcionAFinalPanel("cursadasInscripcionAFinalPanel");
		this.add(this.cursadasInscripcionAFinalPanel);

		this.examenInscripcionAFinalPanel = new ExamenInscripcionAFinalPanel("examenInscripcionAFinalPanel");
		this.add(this.examenInscripcionAFinalPanel);

		this.dniAlumnoInscripcionAFinalPanel.setCarrerasInscripcionAFinalPanel(carrerasInscripcionAFinalPanel);
		this.carrerasInscripcionAFinalPanel.setCursadasInscripcionAFinalPanel(cursadasInscripcionAFinalPanel);
		this.cursadasInscripcionAFinalPanel.setExamenInscripcionAFinalPanel(examenInscripcionAFinalPanel);

	}
}
