package ar.edu.unq.libromatrizdigital.wicket.panel;

import org.apache.wicket.Page;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.form.AjaxButton;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.PropertyModel;

public class DniAlumnoPanel extends Panel {
	private static final long serialVersionUID = -5564221165168905568L;
	private CarrerasPanel carreraPanel;
	private DniAlumnoController alumnoController = new DniAlumnoController();

	public DniAlumnoPanel(String wicket_id) {
		super(wicket_id);

		Form<DniAlumnoController> buscarAlumnoForm = new Form<DniAlumnoController>("buscarAlumnoForm");

		AjaxButton verAlumno = new AjaxButton("verAlumno") {
			private static final long serialVersionUID = -7444106852272624717L;

			@Override
			protected void onSubmit(AjaxRequestTarget target, Form<?> form) {
				carreraPanel.setAlumno(alumnoController.getAlumno());
				carreraPanel.setVisible(true);

				Page laPaginaQueMeContiene = target.getPage();
				laPaginaQueMeContiene.add(carreraPanel);
				target.add(laPaginaQueMeContiene);
			}
		};

		buscarAlumnoForm.add(verAlumno);
		buscarAlumnoForm.add(new TextField<>("dniInput", new PropertyModel<>(alumnoController, "dni")));
		this.add(buscarAlumnoForm);
	}

	public void setCarreraPanel(CarrerasPanel carreraPanel) {
		this.carreraPanel = carreraPanel;
	}

	public DniAlumnoController getAlumnoController() {
		return alumnoController;
	}
}
