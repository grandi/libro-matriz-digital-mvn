package ar.edu.unq.libromatrizdigital.wicket.materia;

import java.io.Serializable;

import ar.edu.unq.libromatrizdigital.model.Materia;

public class ModificarMateriaController implements Serializable {
	private static final long serialVersionUID = -6775317964148969106L;
	private Materia mate;
	private String nombre;
	private Boolean esPromocionable;

	public Materia getMate() {
		return mate;
	}

	public void setMate(Materia mate) {
		this.mate = mate;
		this.mate.getNombre();
		this.mate.esPromocionable();
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Boolean getEsPromocionable() {
		return esPromocionable;
	}

	public void setEsPromocionable(Boolean esPromocionable) {
		this.esPromocionable = esPromocionable;
	}

	public void asignarMateria(Materia materia) {
		if (this.nombre != null && this.esPromocionable != null) {
			materia.setNombre(this.getNombre());
			materia.setEsPromocionable(this.getEsPromocionable());
		}
	}

}
