package ar.edu.unq.libromatrizdigital.wicket.alumno;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import ar.edu.unq.libromatrizdigital.hibernate.OperationHome;
import ar.edu.unq.libromatrizdigital.model.Alumno;
import ar.edu.unq.libromatrizdigital.model.Carrera;
import ar.edu.unq.libromatrizdigital.model.Cursada;
import ar.edu.unq.libromatrizdigital.model.Direccion;
import ar.edu.unq.libromatrizdigital.model.Examen;

public class AlumnoStore {

	private List<Carrera> carreras = new ArrayList<>();
	private List<Alumno> alumnos = new ArrayList<>();
	private List<Cursada> cursadas = new ArrayList<>();
	private List<Examen> examenes = new ArrayList<>();

	private static AlumnoStore unoEspecifico;

	public static AlumnoStore unico() {
		if (unoEspecifico == null) {
			unoEspecifico = new AlumnoStore();
		}
		return unoEspecifico;

	}

	public List<Alumno> getAlumnos() {
		return OperationHome.getInstance().getSession().createQuery("from Alumno", Alumno.class).list();
	}

	public void agregarAlumno(Alumno alumno, Direccion direccion) {
		if (!this.estaEnElListado(alumno.getDni())) {
			this.alumnos.add(alumno);
			OperationHome.getInstance().insert(direccion);
			OperationHome.getInstance().insert(alumno);
		}
	}

	public void agregarExamenFinal(Examen examenFinal) {
		this.examenes.add(examenFinal);
	}

	public List<Examen> getExamenes() {
		return this.examenes;
	}

	public void cargarExamen(Examen _examenes) {
		Examen examen1 = new Examen("final");
		this.examenes.add(examen1);
	}

	public void agregarCarrera(Carrera _carrera) {// esta como lista porque el
													// alumno puede tener mas de
													// una carrera
		this.carreras.add(_carrera);
	}

	public List<Carrera> getCarrera() {
		return this.carreras;
	}

	public void cargarCarrera() {
		Carrera carrera = new Carrera("Programacion Informatica");
		this.agregarCarrera(carrera);
	}

	public void agregarCursada(Cursada _cursada) {
		this.cursadas.add(_cursada);
	}

	public List<Cursada> getCursadas() {
		return cursadas;
	}

	public Alumno getAlumnoConNombre(String _nombre) {
		return this.alumnos.stream().filter(a -> a.getNombre().equals(_nombre)).findAny().get();
	}

	public boolean estaEnElListado(String _dni) {
		return this.alumnos.stream().anyMatch(a -> a.getDni().equals(_dni));
	}

	public Alumno getAlumnoConDni(String _dni) {
//		return this.alumnos.stream().filter(a -> a.getDni().equals(_dni)).findAny().get();
		return OperationHome.getInstance().findByText(_dni, Alumno.class).get(0);
	}

	public List<Cursada> getCursadasDe(Alumno _alumno) {
		return _alumno.getMateriasCursadas();
	}

	public List<Cursada> getMateriasCursadasPor(String _dni) {
		return this.getCursadasDe(this.getAlumnoConDni(_dni)).stream().filter(c -> c.estadoCursando())
				.collect(Collectors.toList());
	}

	public Alumno getAlumnoQueNoEstaCargado(Alumno _alumno) {
		return this.alumnos.stream().filter(a -> a.getDni() != (_alumno.getDni())).findAny().get();
	}

	public void setBajaAlumno(Alumno _alumno) {
//		this.alumnos.remove(_alumno);
		OperationHome.getInstance().delete(_alumno);
	}

	public List<Carrera> getCarrerasDe(Alumno _alumno) {
		return this.carreras.stream().filter(c -> c.getAlumnosInscriptos().equals(_alumno))
				.collect(Collectors.toList());

	}
	
	public void modificarAlumno(Alumno alumno, Direccion direccion) {
			OperationHome.getInstance().update(direccion);
			OperationHome.getInstance().update(alumno);
	}

}
