package ar.edu.unq.libromatrizdigital.wicket.inscripcionafinal;

import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.PropertyModel;

import ar.edu.unq.libromatrizdigital.model.Alumno;
import ar.edu.unq.libromatrizdigital.model.Carrera;
import ar.edu.unq.libromatrizdigital.model.Examen;

public class ExamenInscripcionAFinalPanel extends Panel {
	private static final long serialVersionUID = 3801409908114186711L;
	private Carrera carrera;
	private Alumno alumno;
	private Examen examen;

	private ExamenInscripcionAFinalController controller = new ExamenInscripcionAFinalController();

	public ExamenInscripcionAFinalPanel(String id) {
		super(id);
		this.setVisible(false);
		this.setOutputMarkupId(true);

		Form<ExamenInscripcionAFinalController> buscarExamenForm = new Form<ExamenInscripcionAFinalController>(
				"buscarExamenForm");

		buscarExamenForm.add(new TextField<>("fecha", new PropertyModel<>(this.controller, "fecha")));
		buscarExamenForm.add(new TextField<>("hora", new PropertyModel<>(this.controller, "hora")));

		this.add(buscarExamenForm);
	}

	public void setCarrera(Carrera carrera) {
		this.carrera = carrera;
	}

	public void setAlumno(Alumno alumno) {
		this.alumno = alumno;
	}

	public void setExamen(Examen examen) {
		this.examen = examen;
	}

}