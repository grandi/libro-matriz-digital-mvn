package ar.edu.unq.libromatrizdigital.wicket.materia;

import java.util.ArrayList;
import java.util.List;

import ar.edu.unq.libromatrizdigital.model.Carrera;
import ar.edu.unq.libromatrizdigital.model.Materia;
import ar.edu.unq.libromatrizdigital.model.OfertaAcademicaStore;

public class MateriasStore {
	private static MateriasStore elUnico = new MateriasStore();

	public static MateriasStore unico() {
		if (elUnico == null) {
			elUnico = new MateriasStore();
		}
		return elUnico;
	}

	private List<Materia> materias = new ArrayList<>();

	public MateriasStore() {
		super();
	}

	public void agregarMateria(Materia carreraN) {
		materias.add(carreraN);
	}

	public List<Materia> getMaterias() {
		return materias;
	}

	public void setMaterias(List<Materia> mate) {
		this.materias = mate;
	}

	public void cargarDatosIniciales() {
		Carrera tpi = OfertaAcademicaStore.instituto().getCarreraConNombre("tecnicatura en informatica");
		Materia intro = new Materia("Introducción a la programación", true);
		Materia mate = new Materia("Matematica 1", true);
		Materia estructura = new Materia("Estructuras de datos", true);
		Materia objetos = new Materia("Programación con objetos I", false);

		tpi.agregarMateria(intro);
		tpi.agregarMateria(mate);
		tpi.agregarMateria(estructura);
		tpi.agregarMateria(objetos);

		objetos.agregarCorrelativa(intro);
		objetos.agregarCorrelativa(mate);
		estructura.agregarCorrelativa(objetos);
	}

}
