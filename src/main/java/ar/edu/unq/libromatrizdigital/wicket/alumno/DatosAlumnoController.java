package ar.edu.unq.libromatrizdigital.wicket.alumno;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import ar.edu.unq.libromatrizdigital.model.Alumno;
import ar.edu.unq.libromatrizdigital.model.Carrera;
import ar.edu.unq.libromatrizdigital.model.Direccion;

public class DatosAlumnoController implements Serializable {
	private static final long serialVersionUID = 6785848824087742754L;

	private Alumno alumno;
	private String nombre;
	private String apellido;
	private String dni;
	private String telefono;
	private String calle;
	private String numero;
	private String localidad;
	private String email;
	private List<Carrera> carreras = new ArrayList<>();
	private Carrera carrera;

	public DatosAlumnoController() {
		super();
	}

	public DatosAlumnoController(Alumno alumno) {
		this.alumno = alumno;
		this.nombre = alumno.getNombre();
		this.apellido = alumno.getApellido();
		this.dni = alumno.getDni();
		this.telefono = alumno.getTelefono();
		this.calle = alumno.getDireccion().getCalle();
		this.numero = alumno.getDireccion().getNumero();
		this.localidad = alumno.getDireccion().getLocalidad();
		this.email = alumno.getEmail();

	}

	public Alumno getAlumno() {
		return alumno;
	}

	public void setAlumno(Alumno alumno) {
		this.alumno = alumno;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public String getDni() {
		return dni;
	}

	public void setDni(String dni) {
		this.dni = dni;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getCalle() {
		return calle;
	}

	public void setCalle(String calle) {
		this.calle = calle;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getLocalidad() {
		return localidad;
	}

	public void setLocalidad(String localidad) {
		this.localidad = localidad;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public List<Carrera> getCarreras() {
		return carreras;
	}

	public void setCarreras(List<Carrera> carreras) {
		this.carreras = carreras;
	}

	public Carrera getCarrera() {
		return carrera;
	}

	public void setCarrera(Carrera carrera) {
		this.carrera = carrera;
	}

	public void modificarAlumno() {
		if (this.datosAlumnoBienCargados()) {
			this.setDatos();
			AlumnoStore.unico().modificarAlumno(alumno, alumno.getDireccion());
		}
	}

	public void setDatos() {
		alumno.setApellido(this.getApellido());
		alumno.setNombre(this.getNombre());
		alumno.setDni(this.getDni());
		alumno.setTelefono(this.getTelefono());
		alumno.setEmail(this.getEmail());
		alumno.getDireccion().setCalle(this.getCalle());
		alumno.getDireccion().setNumero(this.getNumero());
		alumno.getDireccion().setLocalidad(this.getLocalidad());
	}
	
	public void removerAlumno() {
		if (this.datosAlumnoBienCargados()) {
			Direccion direccion = new Direccion(this.getCalle(), this.getNumero(), this.getLocalidad());
			Alumno alumno = new Alumno(this.getNombre(), this.getApellido(), this.getDni(), this.getTelefono(),
					direccion, this.getEmail());

			AlumnoStore.unico().setBajaAlumno(alumno);

		}
	}

	public boolean datosAlumnoBienCargados() {
		return this.getNombre() != null && this.getApellido() != null && this.getDni() != null
				&& this.getTelefono() != null && this.getCalle() != null && this.getNumero() != null
				&& this.getLocalidad() != null && this.getEmail() != null;
	}

}
