package ar.edu.unq.libromatrizdigital.wicket.correlativa;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.ChoiceRenderer;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.model.PropertyModel;

import ar.edu.unq.libromatrizdigital.model.Materia;
import ar.edu.unq.libromatrizdigital.wicket.materia.MateriasPage;

public class CorrelativasPage extends WebPage {

	private static final long serialVersionUID = 1L;

	private MateriaElegidaController controller;

	public CorrelativasPage(Materia materiaE) {
		this.controller = new MateriaElegidaController(materiaE);
		this.nombreDeMateriaElegida();
		this.crearTableCorrelativas();
		this.crearFormAgregar();
		this.add(new Link<String>("volver") {
			private static final long serialVersionUID = 1L;

			@Override
			public void onClick() {
				this.setResponsePage(new MateriasPage(controller.getMateriaElegida().getCarrera()));
			};
		});
	}

	private void crearFormAgregar() {
		Form<MateriaElegidaController> agregarMateria = new Form<MateriaElegidaController>("agregarMateria") {
			private static final long serialVersionUID = 5932937158394555903L;

			@Override
			protected void onSubmit() {
				controller.confirmarAgregarCorrelativa();
				this.setResponsePage(new CorrelativasPage(CorrelativasPage.this.controller.getMateriaElegida()));
			}
		};

		agregarMateria.add(new DropDownChoice<>(
				// id
				"materia",
				// binding del valor
				new PropertyModel<>(controller, "nuevaCorrelativa"),
				// binding de la lista de items
				new PropertyModel<>(controller, "materiasCarrera"),
				// que se muestra de cada item
				new ChoiceRenderer<>("nombre")));
		this.add(agregarMateria);
	}

	private void nombreDeMateriaElegida() {
		this.add(new Label("nombreDeMateriaElegida", new PropertyModel<>(this.controller, "materiaElegida.nombre")));
	}

	private void crearTableCorrelativas() {
		this.add(new ListView<Materia>("filaMateria", new PropertyModel<>(this.controller, "correlativas")) {
			private static final long serialVersionUID = 5512414749510820593L;

			@Override
			protected void populateItem(ListItem<Materia> panel) {
				Materia materia = panel.getModelObject();
				panel.add(new Label("nombreCorrelativas", new PropertyModel<>(panel.getModelObject(), "nombre")));

				Link<String> botonVer = new Link<String>("ver") {
					private static final long serialVersionUID = 1L;

					@Override
					public void onClick() {
						this.setResponsePage(new CorrelativasPage(materia));
					}
				};
				panel.add(botonVer);

				Link<String> botonAsignar = new Link<String>("borrar") {
					private static final long serialVersionUID = 1L;

					@Override
					public void onClick() {
						controller.borrarCorrelativa(materia);
					}
				};
				panel.add(botonAsignar);
			}

		});
	}
}
