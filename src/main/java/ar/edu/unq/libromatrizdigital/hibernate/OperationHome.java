package ar.edu.unq.libromatrizdigital.hibernate;

import java.util.List;

import org.hibernate.Session;

public class OperationHome  {

	static private OperationHome instance = new OperationHome();

	public static OperationHome getInstance() {
		return instance;
	}

	public Session getSession() {
		return SessionFactoryContainer.getSessionFactory().getCurrentSession();
	}

	/*
	 * @Override public Maguito findByName(String name) { return
	 * this.getSession().createQuery("FROM Maguito WHERE nombre = :name",
	 * Maguito.class) .setParameter("name", name).getSingleResult();
	 * 
	 * }
	 */

	public <T> T findByName(String name, Class<T> type) {
		 return this.getSession().createQuery("FROM " + type.getSimpleName() + " WHERE nombre = :name",
				  type).setParameter("name", name).getSingleResult();
	}
	
	public <T> List<T> findByText(String text, Class<T> type) {
		 return this.getSession().createQuery("FROM " + type.getSimpleName() + " WHERE apellido = :text "
		 		+ "or dni = :text",
				  type).setParameter("text", text).getResultList();
	}
	
	public <T> T findById(int id, Class<T> type) {
		 return this.getSession().load(type, id);
	}
		
	public <T> void insert(T object) {
		this.getSession().save(object);
	}

	public <T> void update(T object) {
		this.getSession().update(object);
	}

	public <T> void delete(T object) {
		this.getSession().delete(object);
	}
	
}
