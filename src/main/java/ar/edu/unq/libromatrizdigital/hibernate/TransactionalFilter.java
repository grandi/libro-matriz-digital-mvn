package ar.edu.unq.libromatrizdigital.hibernate;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.wicket.protocol.http.WebSession;
import org.hibernate.Session;
import org.hibernate.Transaction;

import ar.edu.unq.libromatrizdigital.model.SessionAttacher;

public class TransactionalFilter implements Filter {

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		SessionFactoryContainer.buildSessionFactory(false);
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		
		try (Session session = SessionFactoryContainer.getSessionFactory().getCurrentSession()) {
			this.manageTransaction(request, response, chain, session);
		}
	}

	private void attachObjects(Session session, HttpSession httpSession) {
		SessionAttacher.getInstance().initSession(session, httpSession);
	}

	private void manageTransaction(ServletRequest request, ServletResponse response, FilterChain chain,
			Session session) throws ServletException, IOException {

		
		Transaction transaction = session.beginTransaction();
		try {
			this.attachObjects(session, ((HttpServletRequest)request).getSession());
			System.out.println("*******************Filtro de tx iniciado");
			chain.doFilter(request, response);
			transaction.commit();
			System.out.println("*******************Commit de tx");
		} catch (IOException | ServletException | RuntimeException e) {
			transaction.rollback();
			throw e;
		}
		finally {
			session.close();
		}

	}

	@Override
	public void destroy() {
		SessionFactoryContainer.getSessionFactory().close();
	}

}